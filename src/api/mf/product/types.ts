export interface ProductVO extends BaseEntity {
  /**
   * 父产品id
   */
  parentId: string | number;

  /**
   * 产品名称
   */
  productName: string;

  /**
   * 显示顺序
   */
  orderNum: number;

  /**
   * 产品状态（0正常 1停用）
   */
  status: string;

  /**
   * 子对象
   */
  children: ProductVO[];
}

export interface ProductForm {
  /**
   * 产品id
   */
  productId?: string | number;

  /**
   * 父产品id
   */
  parentId?: string | number;

  /**
   * 产品名称
   */
  productName?: string;

  /**
   * 显示顺序
   */
  orderNum?: number;

  /**
   * 产品状态（0正常 1停用）
   */
  status?: string;

  /**
   * 乐观锁
   */
  version?: number;

}

export interface ProductQuery {
  /**
   * 产品名称
   */
  productName?: string;

  /**
   * 产品状态（0正常 1停用）
   */
  status?: string;

  /**
   * 日期范围参数
   */
  params?: any;
}
