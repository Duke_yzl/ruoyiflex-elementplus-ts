export interface CustomerVO extends BaseEntity {
  /**
   * 客户姓名
   */
  customerName: string;

  /**
   * 手机号码
   */
  phonenumber: string;

  /**
   * 客户性别
   */
  gender: string;

  /**
   * 客户生日
   */
  birthday: string;

  /** 商品子表信息 */
  goodsList: Array<GoodsVO>;
}

export interface GoodsVO extends BaseEntity {
  /** 列表序号 */
  index:number;
  /**
  * 商品名称
  */
  name: string;

  /**
  * 商品重量
  */
  weight: number;

  /**
  * 商品价格
  */
  price: number;

  /**
  * 商品时间
  */
  date: string;

  /**
  * 商品种类
  */
  type: string;

  /**
  * 乐观锁
  */
  version: number;

}

export interface CustomerForm {
  /**
   * 客户id
   */
  customerId?: string | number;

  /**
   * 客户姓名
   */
  customerName?: string;

  /**
   * 手机号码
   */
  phonenumber?: string;

  /**
   * 客户性别
   */
  gender?: string;

  /**
   * 客户生日
   */
  birthday?: string;

  /**
   * 客户描述
   */
  remark?: string;

  /**
   * 乐观锁
   */
  version?: number;

}

export interface CustomerQuery extends PageQuery {
  /**
   * 客户姓名
   */
  customerName?: string;

  /**
   * 手机号码
   */
  phonenumber?: string;

  /**
   * 客户性别
   */
  gender?: string;

  /**
   * 日期范围参数
   */
  params?: any;
}
