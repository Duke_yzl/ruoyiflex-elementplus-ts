import request from '@/utils/request';
import { AxiosPromise } from 'axios';
import { DemoStudentVO, DemoStudentForm, DemoStudentQuery } from '@/api/demo/student/types';

/**
 * 查询学生信息表列表
 * @param query
 * @returns {*}
 */

export const listStudent = (query?: DemoStudentQuery): AxiosPromise<DemoStudentVO[]> => {
  return request({
    url: '/demo/student/list',
    method: 'get',
    params: query
  });
};

/**
 * 查询学生信息表详细
 * @param studentId
 */
export const getStudent = (studentId: string | number): AxiosPromise<DemoStudentVO> => {
  return request({
    url: '/demo/student/' + studentId,
    method: 'get'
  });
};

/**
 * 新增学生信息表
 * @param data
 */
export const addStudent = (data: DemoStudentForm) => {
  return request({
    url: '/demo/student',
    method: 'post',
    data: data
  });
};

/**
 * 修改学生信息表
 * @param data
 */
export const updateStudent = (data: DemoStudentForm) => {
  return request({
    url: '/demo/student',
    method: 'put',
    data: data
  });
};

/**
 * 删除学生信息表
 * @param studentId
 */
export const delStudent = (studentId: string | number | Array<string | number>) => {
  return request({
    url: '/demo/student/' + studentId,
    method: 'delete'
  });
};
